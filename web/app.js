import CustomerCreationForm from "./src/components/CustomerCreationForm.js";
import CustomerList from "./src/components/CustomerList.js";

const routes = [
  { path: '/', component: CustomerCreationForm,  },
  { path: '/list', component: CustomerList }
];

const router = new VueRouter({
    routes
});

const app = new Vue({
    router
}).$mount('#app');


