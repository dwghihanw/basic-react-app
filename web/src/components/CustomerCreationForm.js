import config from "../config/config.js";
const CustomerCreationForm = {

    data: function () {
        return {
            title: 'Create Customer',
            showSuccessMsg: false,
            form: {
                name: null,
                nic:null,
                address: null,
                telephone: [
                    {
                        telephone: null
                    }
                ]
            },
            errors: []
        }
        
    },
    template: `<div class="row">
            
    <div class="col-lg-12 col-md-12 col-sm-12 mt-1">
        <h2>{{ title }}</h2>
    </div>

    <div v-if="showSuccessMsg" class="col-lg-12 col-md-12">
        <div class="alert alert-primary" role="alert">
            Customer Created Successfully!
        </div>
    </div>

    <div v-if="errors.length" class="col-lg-12 col-md-12 col-sm-12 mt-1">
        <div class="alert alert-danger" role="alert">
            <ul>
                <li v-for="error in errors">{{ error }}</li>
            </ul>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-12 mt-1">
        <label>Name</label>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 mt-1">
        <input v-model="form.name" type="text" name="name" value="" id="name" class="form-control" />
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 mt-1">
        <label>NIC</label>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 mt-1">
        <input v-model="form.nic" type="text" name="nic" value="" id="nic" class="form-control" />
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 mt-1">
        <label>Address</label>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 mt-1">
        <input v-model="form.address" type="text" name="address" value="" id="address" class="form-control" />
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 mt-1">
        <label>Telephone</label>
    </div>
    <div class="col-lg-5 col-md-5 col-sm-12 mt-1">
        <input type="number" v-for="(item, index) in form.telephone" v-model="item.telephone" name="telehone" value="" id="t1" class="form-control mt-1" placeholder="011-xxxxxxx" />
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 mt-1">
        <input type="button" @click="addTelNo" name="addbtn" value="+" id="addbtn" class="btn btn-success" />
        <input type="button" @click="removeTelNo" name="removebtn" value="-" id="removebtn" class="btn btn-danger" />
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 mt-1" style="margin-top: 10px">
        <input type="btn" class="btn btn-success" @click="submit" name="save" id="save" value="Save" />
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        <input type="btn" class="btn btn-danger" name="cancel" @click="cancel" id="cancel" value="cancel"
            style=" margin-top: 10px;" />
    </div>           
</div>`,
    methods: {
        addTelNo: function() {
            this.form.telephone.push({
                telephone: null
            })
        },

        removeTelNo: function() {
            if(this.form.telephone.length > 1) {
                this.form.telephone.pop()
            }
            
        },
        checkForm: function() {

            this.errors = [];
            this.showSuccessMsg = false;

            if (this.form.name && this.form.nic && this.form.address) {
                return true;
            }

            if(!this.form.name) {
                this.errors.push('Name Required.');
            }

            if(!this.form.nic) {
                this.errors.push('NIC Required.');
            }

            if(!this.form.address) {
                this.errors.push('Address Required.');
            }

            let emptyTels = this.form.telephone.filter((item) => {
                return item.telephone ? true : false;
            })

            if(emptyTels.length < 1) {
                this.errors.push('Atleast one telephone number required.');
            }

            if (this.errors.length > 0) {
                return false;
            }
        },

        submit: function(e) {
            if(this.checkForm()) {

                fetch(config.baseUrl+'/api/public/index.php/api/customer', {
                    method: 'POST',
                    body: JSON.stringify(this.form),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                }).then((response) => {
                    if(response.status === 201) {
                        this.resetForm();
                        this.showSuccessMsg = true;
                        // this.$router.push('/list');
                    }           
                    //return response.json();
                }).then((myJson) => {
                    
                });
            }
        },

        cancel: function() {
            this.resetForm();
            this.showSuccessMsg = false;
            this.errors = [];
        },

        resetForm: function() {
            this.form = {
                name: null,
                nic:null,
                address: null,
                telephone: [
                    {
                        telephone: null
                    }
                ]
            }
        }
    }
};

export default CustomerCreationForm;