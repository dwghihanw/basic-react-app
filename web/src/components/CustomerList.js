import config from "../config/config.js";

const CustomerList = {
    data: function() {
        return {
            search: null,
            data: []
        }
    },
    template: `
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
            <label>Search</label>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <input v-model="search" v-on:keyup="keywordSearch" type="text" name="search" value="" id="search" class="form-control" />
        </div>

        <table class="table" style="margin-top: 5px;">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">NIC</th>
                    <th scope="col">Address</th>
                    <th scope="col">Telephone</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(item, index) in data">
                    <th scope="row">{{item.id}}</th>
                    <td>{{item.name}}</td>
                    <td>{{item.nic}}</td>
                    <td>{{item.address}}</td>
                    <td>{{item.telephone}}</td>
                </tr>
            </tbody>
        </table>
    </div>`,
    mounted: function() {

        fetch(config.baseUrl+'/api/public/index.php/api/customer', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {         
            return response.json();
        }).then((json) => {
            this.data = json.data
        });
    },

    methods: {
        keywordSearch: function() {
            fetch(config.baseUrl+'/api/public/index.php/api/customer?keyword='+this.search, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }).then((response) => {         
                return response.json();
            }).then((json) => {
                this.data = json.data
            });
        }
    }
}

export default CustomerList;