<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomerTelephone;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function store(Request $request) {
        try {
            $customer = new Customer();
            $customer->name = $request->post('name');
            $customer->nic = $request->post('nic');
            $customer->address = $request->post('address');
            if($customer->save()) {
                $telRecods = [];
                foreach ($request->post('telephone') as $tel) {
                    array_push($telRecods, ['customer_id'=>$customer->id, 'telephone' => $tel['telephone']]);
                }
                CustomerTelephone::insert($telRecods);
                return response()->json(['data' => $customer, 'success' => true], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['data' => [], 'success' => false, 'msg'=>$e->getMessage()], 500);
        }
        
    }
    
    public function index(Request $request) {
        try {
            $customers = Customer::with('telephone');
            if($request->keyword) {
                $customers->orWhere('name', 'LIKE', "%$request->keyword%");
                $customers->orWhere('nic', 'LIKE', "%$request->keyword%");
                $customers->orWhere('address', 'LIKE', "%$request->keyword%");
            }
            $results = $customers->get();
            $customerList = [];
            foreach ($results as $customer) {
                $telephoneList = [];
 
                if(count($customer->telephone) > 0) {
                    foreach ($customer->telephone as $tel) {
                        array_push($telephoneList, $tel['telephone']);
                    }
                }

                array_push($customerList, [
                    'id' => $customer->id,
                    'name' => $customer->name,
                    'nic' => $customer->nic,
                    'address' => $customer->address,
                    'telephone' => implode(', ', $telephoneList)
                ]);
            }
            return response()->json(['data' => $customerList, 'success' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['data' => [], 'success' => false, 'msg'=>$e->getMessage()], 500);
        }
    }
}
