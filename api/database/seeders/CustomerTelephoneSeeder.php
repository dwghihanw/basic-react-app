<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CustomerTelephoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_telephones')->insert(
            [
                'id' => 1,
                'customer_id' => 1,
                'telephone' => '071443835'
            ]
        );
    }
}
